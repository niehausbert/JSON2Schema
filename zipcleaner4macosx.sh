#!/bin/sh
# Zipping MacOSX contains additional files and folder that consume space in the generated ZIP.
# this script removes those files.
# If you unzip on MacOSX system you will not see those files.
# For cross platform use of ZIP files the additional files like ".DS_store" and "___MACOSX" folder
# are removed with this script.
zip -d $1 __MACOSX/\*
zip -d $1 \*.DS_Store
