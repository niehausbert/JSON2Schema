/* ---------------------------------------
 Exported Module Variable: JSON2Schema
 Package:  json2schema4editor
 Version:  2.2.9  Date: 2020/12/09 8:35:54
 Homepage: https://gitlab.com/niehausbert/JSON2Schema#readme
 Author:   Bert Niehaus
 License:  MIT
 Date:     2020/12/09 8:35:54
 Require Module with:
    const JSON2Schema = require('json2schema4editor');
 JSHint: installation with 'npm install jshint -g'
 ------------------------------------------ */

/*jshint  laxcomma: true, asi: true, maxerr: 150 */
/*global alert, confirm, console, prompt */

/* ---------------------------------------
 Exported Function in Module: getSchema4JSON
 Package:  json2schema4editor
 Version:  0.0.6  Date: 2019/07/30 17:38:08
 Homepage: https://gitlab.com/niehausbert/JSON2Schema#readme
 Author:   Bert Niehaus
 License:  MIT
 Date:     2019/07/30 17:38:08
 Require Module with:
    const getSchema4JSON = require('json2schema4editor');
 JSHint: installation with 'npm install jshint -g'
 ------------------------------------------ */

/*jshint  laxcomma: true, asi: true, maxerr: 150 */
/*global alert, confirm, console, prompt */

function displaySchema(pInputID,pOutputID,pTitleID) {
  var vRootTitle = getValueDOM(pTitleID);

  //var vStringJSON = getEditorValue(pInputID);
  var vStringJSON = ""; //getValueDOM(pInputID);
  var vJSON = null; //getJSON4String(vStringJSON);
  if (vSchemaEditor) {
    vJSON = vSchemaEditor.getValue();
    if (vJSON) {
      var vSchema = getSchema4Editor(vJSON,vRootTitle);
      vSchema.title = vRootTitle;
      vSchema.options.collapsed = false;
      var vStringSchema = JSON.stringify(vSchema,null,4);
      write2value(pOutputID,vStringSchema);
      //setEditorValue(pOutputID,vStringSchema);
      $('#pSchemaOutput').show();
    } else {
      console.error("ERROR: displaySchema('"+pInputID+"','"+pOutputID+"') - Parsing on JSON string had errors");
    }
  } else {
    console.error("ERROR: displaySchema() - vSchemaEditor was not defined!");
  }

}



function onClickSchema4JSON(pInputID,pOutputID,pTitleID) {
  var vRootTitle = getValueDOM(pTitleID);

  //var vStringJSON = getEditorValue(pInputID);
  var vStringJSON = getValueDOM(pInputID);
  var vJSON = getJSON4String(vStringJSON);
  if (vJSON) {
    var vSchema = getSchema4JSON(vJSON,vRootTitle);
    vSchema.title = vRootTitle;
    vSchema.options.collapsed = false;
    var vStringSchema = JSON.stringify(vSchema,null,4);
    write2value(pOutputID,vStringSchema);
    //setEditorValue(pOutputID,vStringSchema);
    $('#pSchemaOutput').show();
  } else {
    console.log("ERROR: onClickSchema4JSON('"+pInputID+"','"+pOutputID+"') - Parsing on JSON string had errors");
  }
}

function getSchema4JSON(pJSON,pTitle) {
  // getSchema4JSON is called for the root element of the JSON file
  var vTitle = pTitle || "MyJSON";
  console.log("getSchema4JSON(pJSON,'"+vTitle+"')");
  var vPath = "";
  var vSchema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "additionalProperties": true,
    "title":vTitle,
    "definitions": {
      "comment": {
          "title": "Comment:",
          "type": "string",
          "format": "textarea",
          "default": ""
      },
      "yesno":{
        "default": "yes",
        "type": "string",
        "enum": [
            "yes",
            "no"
        ]
      }
    }
  };
  var vTypeTree = getTypeTree4JSON(pJSON);
  // vEditorPath is the path to a specific JSON element in the JSON file
  // "root" is the root node of the JSON. "root.name" is addressing the name
  // { "name":"Peter Miller"}. Deeper subelements of the EditorPath will be defined
  // in  a recursive way.
  var vEditorPath = "root";
  // path is keeping track of the JSON schema
  convertJSON2Schema(pJSON,vPath,vSchema,vTypeTree,vEditorPath,vTitle);
  //return vTypeTree;
  return vSchema;
}

function getTypeTree4JSON(pJSON) {
  // clones the original JSON to get the JSON structure of the TypeTree
  var vTypeTree = cloneJSON(pJSON);
  // currently the TypeTree still contains the original default value of pJSON
  // createTypeTree4JSON() replaces the leafs in the TypeTree with the type of leafs
  // arrays and hashes/objects remain untouched
  createTypeTree4JSON(pJSON,vTypeTree);
  return vTypeTree;
}

function toUpperCase1Char(pString) {
  // converts first character to uppercase.
  var vString = pString || "";
  if (vString.indexOf("/")>=0) {
    vString = vString.slice(vString.lastIndexOf("/")+1);
  }
  vString = vString.replace(/[^A-Za-z0-9]/g,"_"); // remove illegial characters in variable name
  return vString.charAt(0).toUpperCase() + vString.slice(1);
}


function getID4EditorPath(pPath) {
  var vID = pPath;
  var vSlashPos = vID.lastIndexOf(".");
  if (vSlashPos>0) {
    vID = pPath.substring(vSlashPos+1);
  }
  return vID;
}

function getTitle4EditorPath(pPath,pType,pRootTitle) {
  var vTitle = getID4EditorPath(pPath);
  if (vTitle.length>0) {
    vTitle = var2title(vTitle);
  }
  //e.g. pPath = root.* vTitle = "*"
  if (vTitle == "*") {
    pPath = pPath.substr(0,pPath.lastIndexOf("."));
    vTitle = getTitle4EditorPath(pPath);
  }
  if (vTitle == "Root") {
    if (pRootTitle) {
      vTitle = pRootTitle;
    }
  }
  if (vTitle.replace(/\s/g,"") == "") {
    vTitle = "Title "+pPath;
  }
  vTitle = var2title(vTitle);
  return vTitle;
}

function var2title(pName) {
  var vName = "undefined var";
  if (pName) {
    vName = pName;
    vName = vName.replace(/[^A-Za-z9-9äöüÄÖÜ]+/g," ");
    var vNameArr = vName.split(" ");
    for (var i = 0; i < vNameArr.length; i++) {
      vNameArr[i] = toUpperCase1Char(vNameArr[i]);
    }
    vName = vNameArr.join(" ");
  }
  return vName;
}

function getDescription4EditorPath(pPath,pType,pDescription) {
  var vDescription = "Description for '"+getID4Path(pPath)+"' Type: '"+pType+"' Path: '"+pPath+"'";
  //vDescription = "";
  if (pDescription) {
    if (pDescription != "") {
      vDescription = pDescription;
    } else {
    }
  }
  return vDescription;
}


function getTitle4JSON(pEditorPath,pType,pTitle) {
  var vTitle = "Title of '"+pEditorPath+"' Type: '"+pType+"'";
  if (pTitle) {
    if (pTitle != "") {
      vTitle = pTitle;
    } else {
      vTitle = getTitle4EditorPath(pEditorPath);
    }
  }
  return vTitle;
}

function createTypeTree4JSON(pJSON,pTypeTree) {
  var vType = getType4JSON(pJSON);
  var vSubType = "";
  switch (vType) {
    //---- OBJECT/HASH -------
    case "object":
      for (var key in pJSON) {
        if (pJSON.hasOwnProperty(key)) {
          // loop over key value pairs of hash
          vSubType = getType4JSON(pJSON[key]);
          if ((vSubType == "array") || (vSubType == "object")) {
            createTypeTree4JSON(pJSON[key],pTypeTree[key]);
          } else {
            pTypeTree[key] = vSubType;
          }
        }
      }
    break;
    //---- ARRAY -------------
    case "array":
      for (var i = 0; i < pJSON.length; i++) {
        vSubType = getType4JSON(pJSON[i]);
        if ((vSubType == "array") || (vSubType == "object")) {
          createTypeTree4JSON(pJSON[i],pTypeTree[i]);
        } else {
          pTypeTree[i] = vSubType;
        }
      }
    break;
    default:
      console.log("createTypeTree4JSON() default - do nothing");
  }
}

function getID4Path(pPath) {
  var vID = pPath;
  var vSlashPos = vID.lastIndexOf("/");
  if (vSlashPos>0) {
    vID = pPath.substring(vSlashPos+1);
  }
  return vID;
}

function getStringDefault(pString) {
  var vDefault = "";
  var options = getEnumOptions(pString);
  if (options.length > 0) {
    vDefault = options[0];
  }
  return vDefault;
}

function getEnumOptions(pString) {
  var vPrefix = "___SELECT___";
  var options = [];
  if (pString) {
    if (pString.indexOf(vPrefix) == 0) {
      // it is a selectbox
      var opt_str = pString.substr(vPrefix.length,pString.length-vPrefix.length);
      options = opt_str.split("|");
    }
  }
  return options;
}

function convertJSON2Schema(pJSON,pPath,pSchema,pTypeTree,pEditorPath,pTitle) {
  //console.log("convertJSON2Schema('"+pPath+"') pTitle='"+pTitle+"'");
  var vTitle = pTitle || "Default Schema Title";
  // vTitle is the Root Title of JSON
  // pTypeTree is need for checking deep equal for "oneOf" definition in arrays
  var vType = getType4JSON(pJSON);
  //---set Type and ID---
  pSchema.type = vType;
  // check if root node of JSON
  if (pPath == "") {
    // replace root ID with a link to JSON
    pSchema.id = "https://niebert.github.io/json-editor";
  } else {
    pSchema.id = pPath;
  }
  //--- create schema dependent on typo of JSON node ----
  switch (vType) {
    //---- OBJECT/HASH -------
    case "object":
      //pSchema.title = "Title of '"+pEditorPath+"' Type: '"+vType+"'";
      pSchema.title = getTitle4EditorPath(pEditorPath,vType,vTitle);
      console.log("Title for Object='"+vTitle+"'");
      pSchema.options = {
          "disable_collapse": false,
          "disable_edit_json": false,
          "disable_properties": false,
          "collapsed": false,
          "hidden": false
      };
      convertObject2Schema(pJSON,pPath,pSchema,pTypeTree,pEditorPath);
    break;
    //---- ARRAY -------------
    case "array":
      pSchema.title = getTitle4EditorPath(pEditorPath,vType,vTitle);
      pSchema.format = "tabs";
      pSchema.options = {
          "disable_collapse": false,
          "disable_array_add": false,
          "disable_array_delete": false,
          "disable_array_reorder": false,
          "disable_properties": false,
          "collapsed": false,
          "hidden": false
      };
      convertArray2Schema(pJSON,pPath,pSchema,pTypeTree,pEditorPath,pSchema.title);
    break;
    //---- STRING ------------
    case "string":
      //pSchema.title = "Title of '"+pEditorPath+"' Type: '"+vType+"'";
      pSchema.title = getTitle4EditorPath(pEditorPath,vType,vTitle);
      pSchema.default = getStringDefault(pJSON); //"Default text of "+vType+" variable";
      // check is default value has prefix "___SELECT___"
      var options = getEnumOptions(pJSON);
      if (options.length > 0) {
        pSchema.enum = options;
        //pSchema.enumTitles = options;
      }
      pSchema.format = determineFormat4String(pJSON);
      pSchema.description = getDescription4EditorPath(pPath,vType);
      //"A description for '"+getID4Path(pPath)+"'  Type: '"+vType+"'";
      pSchema.options = {
          "hidden": false
      };
    break;
    //---- NUMBER ------------
    case "number":
      //pSchema.title = "Title of '"+pEditorPath+"' Type: '"+vType+"'";
      pSchema.title = getTitle4EditorPath(pEditorPath,vType,vTitle);
      pSchema.default = pJSON;
      pSchema.description = "A description for '"+getID4Path(pPath)+"'  Type: '"+vType+"'";
      pSchema.options = {
          "hidden": false
      };
    break;
    //---- INTEGER ------------
    case "integer":
      //pSchema.title = "Title of '"+pEditorPath+"' Type: '"+vType+"'";
      pSchema.title = getTitle4EditorPath(pEditorPath,vType,vTitle);
      pSchema.default = pJSON;
      pSchema.description = "A description for '"+getID4Path(pPath)+"'  Type: '"+vType+"'";
      pSchema.options = {
          "hidden": false
      };
    break;
    //---- BOOLEAN ------------
    case "boolean":
      //pSchema.title = "Title of '"+pEditorPath+"' Type: '"+vType+"'";
      pSchema.title = getTitle4EditorPath(pEditorPath,vType,vTitle);
      pSchema.format = "checkbox";
      pSchema.default = pJSON;
      pSchema.description = "A description for '"+getID4Path(pPath)+"'  Type: '"+vType+"'";
      pSchema.options = {
          "hidden": false
      };
    break;
    default:
      //pSchema.title = "Title of '"+pEditorPath+"' Type: '"+vType+"'";
      pSchema.title = getTitle4EditorPath(pEditorPath,vType,vTitle);
      pSchema.default = null;
      pSchema.description = "A description for '"+getID4Path(pPath)+"'  Type: '"+vType+"'";
      pSchema.options = {
          "hidden": false
      };
    }

    console.log("SchemaGen - Path: '"+pPath+"' Type='"+vType+"' Title='"+pSchema.title+"'");
}

function convertObject2Schema(pJSON,pPath,pSchema,pTypeTree,pEditorPath) {
  // the array of all required keys in the hash/object
  pSchema.defaultProperties = [];
  // properties contains one schema for every key
  pSchema.properties = {};
  var order_index = 10;
  var order_increment = 10;
  for (var key in pJSON) {
    if (pJSON.hasOwnProperty(key)) {
      // set the key as required property in object/hash
      pSchema.defaultProperties.push(key);
      // create the hash for all properties
      pSchema.properties[key] = {};
      // now call convertJSON2Schema() on sub-structure of JSON
      convertJSON2Schema(pJSON[key],pPath+"/properties/"+key,pSchema.properties[key],pTypeTree[key],pEditorPath+"."+key);
      pSchema.properties[key].propertyOrder = order_index;
      order_index += order_increment;
    }
  }
}

function convertArray2Schema(pJSON,pPath,pSchema,pTypeTree,pEditorPath,pItemTitle) {
  var vItemTitle = pItemTitle || "Record";
  var vID = "";
  pSchema.items = {};
  pSchema.items.headerTemplate = vItemTitle+" {{i1}}";
  var vItems = [];
  var vDefaults = [];
  for (var i = 0; i < pJSON.length; i++) {
    // vSubTypeTree contains the TypeTree for the JSON sub structure of pJSON[i]
    // vHash4ID contains the schema for the JSON sub structure of pJSON[i]
    var vHash4ID = {};
    convertJSON2Schema(pJSON[i],pPath+"/items",vHash4ID,pTypeTree[i],pEditorPath+".*");
    // check if previous elements of array are deep equal
    var vDeepEqual = false;
    console.log("Compare JSON1:\n"+JSON.stringify(pTypeTree[i],null,4));
    /* Deep Equal Check
    for (var k = 0; k < i; k++) {
        console.log("Compare JSON2.+k+"]:\n"+JSON.stringify(pTypeTree[k],null,4));
        if(_.isEqual(pTypeTree[k], pTypeTree[i])){
          vDeepEqual = true;
        };
    };
    // if subTypeTree of vHash4ID is not deep equal to previous array elements
    // push the new schema for the element and store the SubTypeTree
    */
    if (vDeepEqual == false) {
        vItems.push(vHash4ID);
    }
  }
  // if more than one vItems are present in array, use "oneOf" for schema.
  if (vItems.length > 1) {
    console.log("ARRAY ONE OF - with " + vItems.length + " different items");
    for (var k = 0; k < vItems.length; k++) {
      vItems[k].id = pPath+"/oneof"+k;
      vItems[k].title = "oneof "+k+" "+pPath;
    }
    pSchema.items.oneOf = vItems;
  } else {
    console.log("ARRAY ONE OF - single element array");
    pSchema.items = vItems[0];
  }
}


function determineFormat4String(pString) {
  var vColorRegEx = new RegExp("^#[0-9a-fA-F]{6}$");
  var vGeolocRegEx = new RegExp("^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$");
  if (vColorRegEx.test(pString) == true) {
    return "color";
  } else if (pString.indexOf("\n") >= 0) {
    return "textarea";
  } else {
    return "text";
  }
}


/* ---------------------------------------
 Exported Function in Module: getTemplate4JSON
 Package:  json2schema4editor
 Version:  0.0.6  Date: 2019/07/30 17:38:08
 Homepage: https://gitlab.com/niehausbert/JSON2Schema#readme
 Author:   Bert Niehaus
 License:  MIT
 Date:     2020/07/22 17:38:08
 Require Module with:
    const getTemplate4JSON = require('json2schema4editor');
 JSHint: installation with 'npm install jshint -g'
 ------------------------------------------ */

/*jshint  laxcomma: true, asi: true, maxerr: 150 */
/*global alert, confirm, console, prompt */

function displayTemplate(pInputID,pOutputID,pTitleID) {
  // this tool uses ACE currently depricated
  var vRootTitle = getValueDOM(pTitleID);

  //var vStringJSON = getEditorValue(pInputID);
  var vStringJSON = ""; //getValueDOM(pInputID);
  var vJSON = null; //getJSON4String(vStringJSON);
  if (vSchemaEditor) {
    vJSON = vSchemaEditor.getValue();
    if (vJSON) {
      // create the JSON Template and store that into the textarea.
      var vSchema = getSchema4Editor(vJSON,vRootTitle);
      vSchema.title = vRootTitle;
      vSchema.options.collapsed = false;
      var vStringSchema = JSON.stringify(vSchema,null,4);
      write2value(pOutputID,vStringSchema);
      //setEditorValue(pOutputID,vStringSchema);
      $('#pTemplateOutput').show();
    } else {
      console.error("ERROR: displayTemplate('"+pInputID+"','"+pOutputID+"') - Parsing on JSON string had errors");
    }
  } else {
    console.error("ERROR: displayTemplate() - vTemplateEditor was not defined!");
  }

}


function getRootTemplate4JSON(pJSON,pTitle,pOutFormat) {
  // getSchema4JSON is called for the root element of the JSON file
  var vTitle = pTitle || "MyJSON";
  console.log("getTemplate4JSON(pJSON,'"+vTitle+"',pJSON,'"+pOutFormat+"')");
  var vPath = "";
  //var vTemplate = "getTemplate4JSON() Title: "+pTitle+"\n\n";
  var vTemplate = "";
  var vJSON = cloneJSON(pJSON);
  //vTemplate += getTemplate4JSON(vTemplate,vJSON);
  // vEditorPath is the path to a specific JSON element in the JSON file
  // "root" is the root node of the JSON. "root.name" is addressing the name
  // { "name":"Peter Miller"}. Deeper subelements of the EditorPath will be defined
  // in  a recursive way.
  var vEditorPath = "root";
  // path is keeping track of the JSON schema
  vTemplate += convertJSON2Template(vJSON,vPath,vTemplate,vEditorPath,vTitle,pOutFormat);
  //return vTypeTree;
  return vTemplate;
}

function getTemplate4JSON(pTemplate,pJSON,pOutFormat) {
  // clones the original JSON to get the JSON structure of the TypeTree
  // currently the TypeTree still contains the original default value of pJSON
  // createTypeTree4JSON() replaces the leafs in the TypeTree with the type of leafs
  // arrays and hashes/objects remain untouched
  pTemplate += createTemplate4JSON(vJSON,pTemplate,pOutFormat);
  return pTemplate;
}

function type4json(pJSON) {
  if (pJSON == null){
    // if (pJSON = null) then typeof(pJSON) return "object".
    // therefore this check is necessary to catch case "undefined" and "null"
    return "null";
  } else if (isArray(pJSON)) {
    return "array"
  } else if (isHash(pJSON)) {
    return "object"
  } else if (typeof(pJSON) == "number") {
    if (isInteger(pJSON)) {
      return "integer"
    } else {
      return "number"
    }
  } else {
    var vType = typeof(pJSON);
    if (vType === "string") {
      if (pJSON.indexOf("___SELECT___") == 0) {
        vType = "select";
      }
    }
    return vType;
  }
};


function createTemplate4JSON(pJSON,pTemplate,pOutFormat) {
  var vType = getType4JSON(pJSON);
  var vSubType = "";
  switch (vType) {
    //---- OBJECT/HASH -------
    case "object":
      for (var key in pJSON) {
        if (pJSON.hasOwnProperty(key)) {
          // loop over key value pairs of hash
          vSubType = getType4JSON(pJSON[key]);
          if ((vSubType == "array") || (vSubType == "object")) {
            pTemplate += createTemplate4JSON(pJSON[key],"",pOutFormat);
          }
        }
      }
    break;
    //---- ARRAY -------------
    case "array":
      for (var i = 0; i < pJSON.length; i++) {
        vSubType = getType4JSON(pJSON[i]);
        if ((vSubType == "array") || (vSubType == "object")) {
          pTemplate += createTemplate4JSON(pJSON[i],"",pOutFormat);
        }
      }
    break;
    default:
      console.log("createTemplate4JSON() default - do nothing");
  }
  return pTemplate
}

function getID4Path(pPath) {
  var vID = pPath;
  var vSlashPos = vID.lastIndexOf("/");
  if (vSlashPos>0) {
    vID = pPath.substring(vSlashPos+1);
  }
  return vID;
}


function getOutTPL(pOutFormat) {
  var vChecked = document.getElementById("cTemplateComments").checked

  console.log("CALL: getOutTPL('" + pOutFormat + "')");
  var vOut = {
    "format": "text",
    "comment":{
        "show": vChecked,
        "prefix":" ",
        "postfix":" "
    },
    "item":{
        "prefix":"\n  * ",
        "postfix":" "
    },
    "itemize": {
        "prefix":"\n  ",
        "postfix":"\n "
    },
    "enumerate": {
        "prefix":"\n ",
        "postfix":"\n "
    }
  };
  vOut.format = pOutFormat;
  switch (pOutFormat) {
    case "html":
      vOut = {
        "format": pOutFormat,
        "comment":{
            "show": vChecked,
            "prefix":"<!-- ",
            "postfix":" -->"
        },
        "item":{
            "prefix":"\n<LI class=\"item4list\"> ",
            "postfix":"\n</LI>"
        },
        "itemize": {
          "prefix":"\n<UL class=\"itemlist\"> ",
          "postfix":"\n</UL>"
        },
        "enumerate": {
          "prefix":"\n<OL class=\"enumlist\"> ",
          "postfix":"\n</OL>"
        }
      }
    break;
    case "markdown":
      vOut = {
        "format": pOutFormat,
        "comment":{
            "show": vChecked,
            "prefix":"<!-- ",
            "postfix":" -->"
        },
        "item":{
            "prefix":"\n* ",
            "postfix":" "
        },
        "itemize": {
          "prefix":"\n ",
          "postfix":"\n "
        },
        "enumerate": {
          "prefix":"\n ",
          "postfix":"\n "
        }
      }
    break;
    case "wiki":
      vOut = {
        "format": pOutFormat,
        "comment":{
            "show": vChecked,
            "prefix":"<!-- ",
            "postfix":" -->"
        },
        "item":{
            "prefix":"\n* ",
            "postfix":" "
        },
        "itemize": {
          "prefix":"\n ",
          "postfix":"\n "
        },
        "enumerate": {
          "prefix":"\n ",
          "postfix":"\n "
        }
      }
    break;
    case "latex":
      vOut = {
        "format": pOutFormat,
        "comment":{
          "show": vChecked,
          "prefix":"% ",
          "postfix":" "
        },
        "item":{
          "prefix":"\n  \\item ",
          "postfix":" "
        },
        "itemize": {
          "prefix":"\n\\begin{itemize}",
          "postfix":"\n\\end{itemize}"
        },
        "enumerate": {
          "prefix":"\n\\begin{enumerate}",
          "postfix":"\n\\end{enumerate}"
        }
      };
    break;
    default:
      //vOut.format = "text";
      console.log("Output Default format 'text'");
  }
  return vOut;
}


function getComment4EditorPath(pEditorPath,pType,pTitle,pOutFormat) {
  var vTemplate = "";
  vTitleTPL = getTitle4EditorPath(pEditorPath,pType,pTitle);
  console.log("CALL: getOutTPL('"+pOutFormat+"') in getComment4EditorPath('" + pEditorPath + "')");
  var vOutTPL = getOutTPL(pOutFormat);
  if (vOutTPL.comment.show == true) {
    vComment = "\n" + vOutTPL.comment.prefix + "Title: " + vTitleTPL +"  Type: "+pType + " " + vOutTPL.comment.postfix;
    vComment += "\n" + vOutTPL.comment.prefix + "ID:    " + pEditorPath + " " + vOutTPL.comment.postfix;
  };
  console.log("Title='" + vTitleTPL + "'");
  return vTemplate;
}

function getTemplate4EditorPath(pEditorPath,pType,pTitle,pOutFormat) {
  var vTemplate = "";
  vTemplate += "{{{" + getID4EditorPath(pEditorPath) + "}}}";
  vTemplate += getComment4EditorPath(pEditorPath,pType,pTitle,pOutFormat)
  return vTemplate;
}
// call convertJSON2Template(pJSON,vPath,vTemplate,vEditorPath,vTitle,pOutFormat)
function convertJSON2Template(pJSON,pPath,pTemplate,pEditorPath,pTitle,pOutFormat) {
  //console.log("convertJSON2Schema('"+pPath+"') pTitle='"+pTitle+"'");
  var vTitle = pTitle || "Default Schema Title";
  // vTitle is the Root Title of JSON used for Schema
  // pTypeTree is need for checking deep equal for "oneOf" definition in arrays
  //var vType = getType4JSON(pJSON);
  var vType = type4json(pJSON);
  // check if root node of JSON
  var vID = "ID?";
  var vTitleJSON = "Title";
  if (pPath == "") {
    // replace root ID with a link to JSON
    vID = "";
  } else {
    vID = getID4EditorPath(pPath);
  };

  console.log("CALL: getOutTPL('"+pOutFormat+"') in convertJSON2Template('" + pPath + "')");
  var vOutTPL = getOutTPL(pOutFormat);
  var vComment = "";
  //--- create schema dependent on typo of JSON node ----
  switch (vType) {
    //---- OBJECT/HASH -------
    case "object":
      //pSchema.title = "Title of '"+pEditorPath+"' Type: '"+vType+"'";
      vTitleTPL = getTitle4EditorPath(pEditorPath,vType,vTitle,pOutFormat);
      console.log("Title for Object='" + vTitleTPL + "'");
      if (vOutTPL.comment.show == true) {
        vComment = vOutTPL.comment.prefix + "Object: " + pEditorPath + vOutTPL.comment.postfix;
      }
      pTemplate += vOutTPL.itemize.prefix + " " + vComment;
      pTemplate += convertObject2Template(pJSON,pPath," ",pEditorPath,vTitle,pOutFormat);
      pTemplate += vOutTPL.itemize.postfix + " " + vComment;
    break;
    //---- ARRAY -------------
    case "array":
      vTitleTPL = getTitle4EditorPath(pEditorPath,vType,vTitle,pOutFormat);
      console.log("Title for Array='" + vTitleTPL + "'");
      if (vOutTPL.comment.show == true) {
        vComment = vOutTPL.comment.prefix  + "Array: "+ pEditorPath + vOutTPL.comment.postfix;
      }
      pTemplate += vOutTPL.enumerate.prefix + " " + vComment;
      pTemplate += convertArray2Template(pJSON,pPath," ",pEditorPath,vTitle,vID,pOutFormat);
      pTemplate += vOutTPL.enumerate.postfix + " " + vComment;
    break;
    //---- STRING ------------
    case "string":
      pTemplate += getTemplate4EditorPath(pEditorPath,vType,vTitle,pOutFormat);
      if (vOutTPL.comment.show == true) {
        pTemplate += "\n " + vOutTPL.comment.prefix + "String Format: " + determineStringFormat4Template(pJSON) + vOutTPL.comment.postfix;
      }
    break;
    //---- SELECT ------------
    case "select":
      var vOptions = getEnumOptions(pJSON);
      if (vOptions.length > 0) {
        console.log("SELECT found for template '" + pJSON + "'");
        for (var i = 0; i < vOptions.length; i++) {
          pTemplate += "\n{{#ifcond " + getID4EditorPath(pEditorPath) + " \"==\" \"" + vOptions[i] + "\"}}"
          pTemplate += "\n  {{{" + getID4EditorPath(pEditorPath) + "}}}";
          if (vOutTPL.comment.show == true) {
            pTemplate += "\n"  + "  " + vOutTPL.comment.prefix + "  Attribute '" + getID4EditorPath(pEditorPath) + "' has value '" + vOptions[i] + "' in output format '" + vOutTPL.format + "'" + vOutTPL.comment.postfix;
          };
          pTemplate += "\n{{/ifcond}}";
        }
        pTemplate += getComment4EditorPath(pEditorPath,"string",pTitle,pOutFormat)
      } else {
        pTemplate += getTemplate4EditorPath(pEditorPath,vType,vTitle,pOutFormat);
        if (vOutTPL.comment.show == true) {
          pTemplate += "\n " + vOutTPL.comment.prefix + "String Format: " + determineStringFormat4Template(pJSON) + vOutTPL.comment.postfix;
        }
      }
    break;
  //---- NUMBER ------------
    case "number":
      //pSchema.title = "Title of '"+pEditorPath+"' Type: '"+vType+"'";
      pTemplate += getTemplate4EditorPath(pEditorPath,vType,vTitle,pOutFormat)
    break;
    //---- INTEGER ------------
    case "integer":
      //pSchema.title = "Title of '"+pEditorPath+"' Type: '"+vType+"'";
      pTemplate += getTemplate4EditorPath(pEditorPath,vType,vTitle,pOutFormat)
    break;
    //---- BOOLEAN ------------
    case "boolean":
      //pSchema.title = "Title of '"+pEditorPath+"' Type: '"+vType+"'";
      pTemplate += "\n{{#if " + getID4EditorPath(pEditorPath) + "}}";
      pTemplate += "\n       " + vOutTPL.comment.prefix + " "  + pEditorPath + "=true " + vOutTPL.comment.postfix;
      pTemplate += "\n{{else}}";
      pTemplate += "\n       " + vOutTPL.comment.prefix + " " + pEditorPath + "=false " + vOutTPL.comment.postfix;
      pTemplate += "\n{{/if}}";
    break;
    default:
      //pSchema.title = "Title of '"+pEditorPath+"' Type: '"+vType+"'";
      pTemplate += "\n{{{" + getID4EditorPath(pEditorPath) + "}}}";
    }

    console.log("TemplateGen - Path: '"+pPath+"' Type='"+vType+"'");
    return pTemplate;
}

function getTemplateID4Path(pPath) {
  var vID = pPath;
  if (pPath) {
    // remove last dot
    vID = vID.replace(/\.^/g,'');
    var vSlashPos = vID.lastIndexOf(".");
    if (vSlashPos>0) {
      vID = pPath.substring(vSlashPos+1);
    }
  } else {
    vID = "root"
  }
  switch (vID) {
    case "*":
        vID = "this";
    break;
    case "root":
        vID = ".";
    break;
    default:
        // getID4Path() generic return
  }
  console.log("getTemplateID4Path(" + pPath + ")='" + vID + "'");
  return vID;
}


function convertObject2Template(pJSON,pPath,pTemplate,pEditorPath,pTitle,pOutFormat) {
  // the array of all required keys in the hash/object
  console.log("CALL: getOutTPL('"+pOutFormat+"') in convertObject2Template('" + pPath + "')");
  var vOutTPL = getOutTPL(pOutFormat);
  var vID = getTemplateID4Path(pEditorPath);
  if (vID && vID != ".") {
    pTemplate += "\n{{#with " + vID + "}}"
  }
  for (var key in pJSON) {
    if (pJSON.hasOwnProperty(key)) {
      pTemplate += vOutTPL.item.prefix;
      // now call convertJSON2Schema() on sub-structure of JSON
      var vSubType = getType4JSON(pJSON[key]);
      pTemplate += convertJSON2Template(pJSON[key],pPath+"/properties/"+key, " " ,pEditorPath+"."+key,pTitle,pOutFormat);
      pTemplate += vOutTPL.item.postfix;
    }
  }
  if (vID && vID != ".") {
    pTemplate += "\n{{/with}}";
  }
  return pTemplate
}

function convertArray2Template(pJSON,pPath,pTemplate,pEditorPath,pTitle,pID,pOutFormat) {
  //var vID = pID || "."; // "." means the root element of the JSON is an array
  var vID = getTemplateID4Path(pPath); // "." means the root element of the JSON is an array
  var vItems = [];
  console.log("CALL: getOutTPL('"+pOutFormat+"') in convertArray2Template('" + pPath + "')");
  var vOutTPL = getOutTPL(pOutFormat);
  if (vOutTPL.comment.show == true) {
    pTemplate += "\n" + vOutTPL.comment.prefix + "Array Path: " + pEditorPath + " "+ vOutTPL.comment.postfix;
  };
  for (var i = 0; i < pJSON.length; i++) {
    pTemplate += "\n{{#each " + getTemplateID4Path(pEditorPath) + "}}"
    pTemplate += vOutTPL.item.prefix;
    var vSubType = getType4JSON(pJSON[i]);
    switch (vSubType) {
      case "array":
        pTemplate += "Sub-Type of Array Element: '"+vSubType+"'";
        pTemplate += convertJSON2Template(pJSON[i],pPath+"/items", "",pEditorPath+".*",pTitle,pOutFormat)
      break;
      case "object":
        pTemplate += "Sub-Type of Array Element: '"+vSubType+"'";
        pTemplate += convertJSON2Template(pJSON[i],pPath+"/items", "",pEditorPath+".*",pTitle,pOutFormat)
      break;
      default:
        pTemplate += "{{{this}}}";
        pTemplate += getComment4EditorPath(pEditorPath,vSubType,pTitle,pOutFormat);
    }
    pTemplate += vOutTPL.item.postfix;
    pTemplate += "\n{{/each}}";
  }
  return pTemplate
}


function determineStringFormat4Template(pString) {
  var vColorRegEx = new RegExp("^#[0-9a-fA-F]{6}$");
  var vGeolocRegEx = new RegExp("^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$");
  if (vColorRegEx.test(pString) == true) {
    return "color";
  } else if (pString.indexOf("\n") >= 0) {
    return "textarea";
  } else {
    return "text";
  }
}


// Geneate the expport modules
JSON2Schema = {
  "getSchema4JSON": getSchema4JSON,
  "getTemplate4JSON": getTemplate4JSON
};



// -------NPM Export Variable: JSON2Schema---------------
module.exports = JSON2Schema;