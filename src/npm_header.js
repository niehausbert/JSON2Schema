/* ---------------------------------------
 Exported Module Variable: JSON2Schema
 Package:  json2schema4editor
 Version:  2.2.9  Date: 2020/12/09 8:35:54
 Homepage: https://gitlab.com/niehausbert/JSON2Schema#readme
 Author:   Bert Niehaus
 License:  MIT
 Date:     2020/12/09 8:35:54
 Require Module with:
    const JSON2Schema = require('json2schema4editor');
 JSHint: installation with 'npm install jshint -g'
 ------------------------------------------ */

/*jshint  laxcomma: true, asi: true, maxerr: 150 */
/*global alert, confirm, console, prompt */
