

<h2>JSON2Schema</h2>
JSON2Schema is a small WebApp running totally in your browser, that supports developers in creating a JSON Schema for the JSON Editor [originally developed by Jeremy Dorn](https://github.com/jdorn/json-editor) and is now available and maintained as [`json-editor`](https://www.github.com/json-editor/json-editor) package in https://www.github.com/json-editor/json-editor

The corresponding NPM module `json2schema4editor` (JSON to(2) Schema for(4) Editor means that the  JSON schema is created for the JSON editor  `json-editor`](https://www.github.com/json-editor/json-editor).

## Online Demo JSON2Schema
You can create from a JSON of your choice a corresponding JSON schema with one click. With for the the JSON2Schema tool with the following URL:

https://niehausbert.gitlab.io/JSON2Schema

Copy a JSON of your choice into the JSON2Schema tool and create a raw JSON schema, that [works in the JSON editor](https://www.github.com/json-editor/json-editor)

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Common Pitfalls](#common-pitfalls)
- [Quickstart](#quickstart)
- [Use with NodesJS](#use-with-nodesjs)
- [What is a JSON Schema?](#what-is-a-json-schema)
- [What is a Handlebars4Code Template?](#what-is-a-handlebars4code-template)
- [Submit JSON to a Restful API](#submit-json-to-a-restful-api)
- [How Create a JSON Schema for a specific WebApp](#how-create-a-json-schema-for-a-specific-webapp)
  - [Learning Task Wikiversity AppLSAC](#learning-task-wikiversity-applsac)
- [ZIP File `jsoneditor_app.zip`](#zip-file-jsoneditor_appzip)
  - [Application of `jsoneditor_app.zip` in JSON2Schema](#application-of-jsoneditor_appzip-in-json2schema)
  - [Exchange `jsoneditor_app.zip` for JSON Editor Generation](#exchange-jsoneditor_appzip-for-json-editor-generation)
    - [(1) Modify the Editor jsoneditor\_app.zip](#1-modify-the-editor-jsoneditor%5C_appzip)
    - [(2) Modify the JSON2Schema Tool in JSON2Schema-master.zip](#2-modify-the-json2schema-tool-in-json2schema-masterzip)
    - [Why is the base64 encoded used for JSON2Schema?](#why-is-the-base64-encoded-used-for-json2schema)
- [Links/Downloads](#linksdownloads)
- [Acknowledgement](#acknowledgement)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->
## Common Pitfalls
* **(Unzip Generated ZIP)** The `Export JSON Editor as ZIP` button creates a ZIP file and will be saved in your download folder. If you try to start the `index.html` directly from  the compressed ZIP file, the JSON editor will not work. Unzip/uncompress all files first and load the `index.html` from the extracted folder.

## Quickstart
* Select a JSON of your choice. If your JSON contains an array, The array should contain just a **single** element. If the array contains more than one element, all elements are treated as `oneOf` element in the JSON schema.
* **(WebApp)** start https://niehausbert.gitlab.io/JSON2Schema in your browser and copy your JSON into the editor **JSON Input**
* press button **create JSON Schema** and you will see the generated JSON schema the next ACE Editor.
* The labels and description for the input elements are generated automatically, so change the label and descriptions for the input data according to your needs.
* Save your updated JSON schema by pressing the **export JSON Schema** button, so that you can use it with your implementation of your [JSON editor](https://www.github.com/json-editor/json-editor).
* **(Preview)** press the **create JSON Editor** to see how your JSON editor will look like.
* under the JSON Editor you will find an export button. Save `jsoneditor_app.zip` by pressing the **Export JSON Editor** button.

## Use with NodesJS
If you want to create a JSON Schema for the [JSON editor by Jeremy Dorn](https://github.com/json-editor/json-editor) with NodeJS you need to install the tool for your project with:
```
npm install json2schema4editor --save
```
Then you can require the module `json2schema4editor` in your script:

```javascript
const j2s = require('json2schema4editor');
let schema_title = "My Title for the Schema";
let my_json = {
  "firstname": "Bert",
  "lastname": "Bond",
  "gender": "___SELECT___male|female|other",
  "email": "noname@example.com"
};
let my_schema = j2s.getSchema4JSON(my_json,schema_title);
```
The module contains also a generator for a draft template for [Handlebars](https://handlebarsjs.com/) resp. [Handelbars4Code](https://gitlab.com/niehausbert/handlebars4code).

## What is a JSON Schema?
A JSON Schema is a basically a file, that defines the type and the layout of the input in the JSON editor. The input elements are defined for each key-value pair (e.g. firstname="Bert" in a given JSON. In this case we use the [JSON Editor by Jeremy Dorn](https://www.github.com/json-editor/json-editor).  I.e. the JSON Schema tells the editor that the user of the JSON editor:
* Edit a string value with a select box (e.g. for male/female/other),
* Edit a string with a free text input as a single line.
* Edit a string with a free text input with multiple lines (e.g. with syntax highlighting for a specific programming language).
* uses a select box for different options in the graphical user interface GUI,
* select a color,
* select a data,
* increase or decrease an integer value, with a specific range and check validity with regular expressions for the input element,
* ...

## What is a Handlebars4Code Template?
Javascript template engines take a JSON as input and fill a template with the content of the JSON. The template contains markers like ``{{firstname}}`` and the JSON contains the data for replacement of the marker with the specific content from the JSON (e.g. `Paul` or `Anna`). With a Javascript template engines like
* Mustache,
* Handelbars,
* ...
you define the structure and syntax of the output.
[Handlebars4Code](https://github.com/niebert/Handlebars4Code) extends [Handlebars Template Engine](https://handlebarsjs.com/) with additional HandlebarsJS helpers for code generation. [Handlebars4Code](https://github.com/niebert/Handlebars4Code) is integrated in [JSON2Schema](https://niehausbert.gitlab.io/JSON2Schema) to add a raw template for output by generated [AppLSAC](https://en.wikiversity.org/wiki/AppLSAC), which runs offline in your browser as runtime environment for Javascript code without the need perform a generation of code on the server side.

## Submit JSON to a Restful API
The generated ZIP contains a WebApp ([AppLSAC](https://en.wikiversity.org/wiki/AppLSAC)) that runs in your browser. That means you should save the edited JSON with the Save button. Run the AppLSAC Firefox and create the `<form>` tag in der pages with the stringified JSON in a `textarea` HTML element, which will be nested  as submitted element in the `form` tag.

**Recommendation:** Add a remark on that page in the WebApp that indicates to which server you want to submit the data and add the privacy statements. Do not create hidden calls from and to remote servers that are not transparent for the users.


## How Create a JSON Schema for a specific WebApp
[JSON2Schema](https://niehausbert.gitlab.io/JSON2Schema) takes a JSON as input and creates a draft version of a JSON Schema, that can be used in the [JSON Editor](https://www.github.com/json-editor/json-editor).

As in example we create a privacy-friendly WebApp, that stores health related private data WITHOUT sending the collected data to a server for [commercial data harvesting](https://en.wikiversity.org/wiki/Commercial_Data_Harvesting) (like fitness tracker).

At first we define a JSON that is capable to store the desired data for your [application](https://en.wikiversity.org/wiki/AppLSAC).
```json
{
  "name":"Hans Mustermann",
  "age":36,
  "weightlist": [
    {
      "date":"2018-05-23",
      "value":76.0
    }
  ],
  "temperaturelist": [
    {
      "date":"2018-05-24",
      "value":36.5
    }
  ]
}
```
 A patient measure temperature and weight on a daily basis. The collected data is stored in the mobile device in JSON file without connecting a server. The storage format of the JSON is defined by a JSON Schema. If you plugin the JSON Schema into the JSON Editor originally generated by Jeremy Dorn, then the JSON editor will provide the Graphical User Interface (GUI) to input the data. Creating a WebApp in general or using the JSON Editor does to specify does not imply that the collected data is not submitted from the mobile device to remote server. OpenSource architecture of the WebApp assures that the computer scientist can validate, if the source code contains hidden components that submit the collected JSON data to remote server.  For a privacy friendly WebApp for the data mentioned above could be stored (even encrypted) on the SD-card of the mobile device or in the local storage of the browser (see [AppLSAC](https://en.wikiversity.org/wiki/AppLSAC)).

### Learning Task Wikiversity AppLSAC
Use the [sample JSON in the Demo JSON2Schema](https://niehausbert.gitlab.io/JSON2Schema) WebApp to play around with the Schema generation and edit the generate Schema to understand how the JSON Schema defines the Graphical User Interface of the JSON Editor.

## ZIP File `jsoneditor_app.zip`
JSON2Schema uses the following [underlying ZIP
file `jsoneditor_app.zip`](https://niehausbert.gitlab.io/JSON2Schema/zip/jsoneditor_app.zip) to bundle the generated JSON schema with a complete JSON editor.

**Remark:** The following sections are designed for developers to understand the underlying principles of JSON2Schema

### Application of `jsoneditor_app.zip` in JSON2Schema
[JSON2Schema](https://niehausbert.gitlab.io/JSON2Schema) loads the compressed JSON editor in [`zip/jsoneditor_app.zip`](https://niehausbert.gitlab.io/JSON2Schema/zip/jsoneditor_app.zip) with the JS library in [`docs/db/files4json.js`](https://niehausbert.gitlab.io/JSON2Schema/db/files4json.js). The file `files2json.js` stores `jsoneditor_app.zip` as [base64 encoded string]. If source of the `ZIP` file is itself a `git`-repository so that users of the JSON2Schema can
* assess the content of the `ZIP` file in the repository https://www.github.com/niebert/jsoneditor_app
* replace the default [`zip/jsoneditor_app.zip`](https://niehausbert.gitlab.io/JSON2Schema/zip/jsoneditor_app.zip) by a user-defined editor that uses the JSON schema generated with [JSON2Schema](https://niehausbert.gitlab.io/JSON2Schema).
* [`docs/db/files4json.js`](https://niehausbert.gitlab.io/JSON2Schema/db/files4json.js) is a defined as library, so that the data can be loaded just by a Javascript `script`-tag.


### Exchange `jsoneditor_app.zip` for JSON Editor Generation

[JSON2Schema](https://niehausbert.gitlab.io/JSON2Schema/) is able to export a complete JSON editor with the generated JSON schema. This tool `create_files4json.html` can be used to alter the pre-defined JSON editor for the exported [jsoneditor\_app.zip](https://niehausbert.gitlab.io/JSON2Schema/zip/jsoneditor_app.zip). If you start [JSON2Schema](https://niehausbert.gitlab.io/JSON2Schema/) an base64 encoded ZIP file is handled with [JSZip](https://stuk.github.io/jszip/) and the generated JSON schema replaces `jsoneditor_app/schema/schema4json.js` in the exported ZIP file. If you want to change the exported JSON editor (e.g. CSS and additional features) perform the following two main steps:


#### (1) Modify the Editor [jsoneditor\_app.zip](https://niehausbert.gitlab.io/JSON2Schema/zip/jsoneditor_app.zip)

* download [underlying ZIP file **`jsoneditor_app.zip`**](https://niehausbert.gitlab.io/JSON2Schema/zip/jsoneditor_app.zip) that is used by [JSON2Schema](https://niehausbert.gitlab.io/JSON2Schema)
* unzip `jsoneditor_app.zip`
* modify the content in the folder `jsoneditor_app/` according to your requirements and constraints.
* compress the folder `jsoneditor_app/` and generate a new `jsoneditor_app.zip`.

#### (2) Modify the JSON2Schema Tool in [JSON2Schema-master.zip](https://gitlab.com/niehausbert/JSON2Schema/-/archive/master/JSON2Schema-master.zip)

* download [ZIP file for JSON2Schema](https://gitlab.com/niehausbert/JSON2Schema/-/archive/master/JSON2Schema-master.zip) from GitLab,
* unzip [JSON2Schema-master.zip](https://gitlab.com/niehausbert/JSON2Schema/-/archive/master/JSON2Schema-master.zip) and
* start the [AppLSAC](https://en.wikiversity.org/wiki/AppLSAC) by loading [`JSON2Schema/docs/create_files4json.html`](https://niehausbert.gitlab.io/JSON2Schema/) in your browser and press the `Add file to JSON Dialog` button for loading your new `jsoneditor_app.zip`
* Press Create Files4JSON tool and load your new generated ZIP-file `jsoneditor_app.zip` and generate a new file.
* replace the generated file **`files4json.js`** in the folder **`JSON2Schema/docs/db/files4json.js`** with your new [base64 encoded](https://en.wikipedia.org/wiki/Base64) ZIP-file **`jsoneditor_app.zip`**.

#### Why is the [base64 encoded](https://en.wikipedia.org/wiki/Base64) used for JSON2Schema?
A zip-file is a binary format and the privacy friendly design of an [AppLSAC](https://en.wikiversity.org/wiki/WebApps_with_LocalStorage_and_AppCache) prevents the browser from reading and writing data silently arbitrary file from the user's hard drive.

[Base64 encoding of zip-files](https://en.wikipedia.org/wiki/Base64) converts a binary file into a string/text.  Binary-to-text encoding and decoding algorithms allow binary data to be stored or carried across channels that support text content without loss of bytes.

[Base64 encoding of zip-files](https://en.wikipedia.org/wiki/Base64) is used to represent the `zip`-file `jsoneditor_app.zip` in string that is stored in a Javascript library  `files2json.js` that stores the Base64 encoded binary as text.

Assume the variable `base64data` contains the [base64 encoded zip-file](https://en.wikipedia.org/wiki/Base64). With `JSZip` the `zip`-file can be populated with base64 encoded zip-file in  `base64data`.

```javascript
var zip = new JSZip();
var base64data = "AOSDIOhajSDAHIOAPSDAIS....";
zip.loadAsync(base64data,{"base64":true}).then(function (pzip) {
  pzip.forEach(function (relativePath, file){
      console.log("File in ZIP: ", relativePath);
  });
});
```
**Remark:** In the demo code above the variable `base64data` does not contain a real base64 encoded `zip`-file. Use the tool [`create_files4json.html`](https://niehausbert.gitlab.io/JSON2Schema/create_files4json.html) to create base64 encoded JavaScript files that stores the [`jsoneditor_app.zip`](https://www.gitlab.com/niebert/jsoneditor_app/), which is the ZIP file of the repository [](https://www.github.com/niebert/jsoneditor_app/)

## Links/Downloads
* **Repository:** [`jsoneditor_app`](https://www.github.com/niebert/jsoneditor_app/) on GitHub - URL: https://www.github.com/niebert/jsoneditor_app/
* **ZIP-file:** [`jsoneditor_app.zip`](https://niehausbert.gitlab.io/JSON2Schema/zip/jsoneditor_app.zip) that was base64 encoded with [`create_files4json.html`](https://niehausbert.gitlab.io/JSON2Schema/create_files4json.html) and save as [`files4json.js`](https://niehausbert.gitlab.io/JSON2Schema/db/files4json.js)
* **Base64 encoded ZIP-File:** [`files4json.js`](https://niehausbert.gitlab.io/JSON2Schema/db/files4json.js) - URL: https://niehausbert.gitlab.io/JSON2Schema/db/files4json.js - `docs/db/files4json.js` stores the `jsoneditor_app.zip` in as base64 encoded string.
* **WebApp:** [`JSON2Schema`](https://gitlab.com/niehausbert/JSON2Schema/-/archive/master/JSON2Schema-master.zip) as ZIP file to run JSON2Schema offline as [AppLSAC-2](https://en.wikiversity/org/wiki/AppLSAC/Types).


## Acknowledgement
Special thanks to the following individual developers and teams of OpenSource JavaScript projects:
* [`LoadFile4DOM`](https://www.gitlab.com/niehausbert/loadfile4dom) is a library that allows to load files into an application that run completely in a browser without the need to submit data to a server for processing. With this library the users are able load files into your browser application and process the data in the browser and provide the output to the user, without submitting any data to a server. **[Demo LoadFile4DOM](https://niehausbert.gitlab.io/loadfile4dom)**
* [FileSaver.js](https://github.com/eligrey/FileSaver.js) Developer Eli Grey provided the `FileSaver.js` that is used to store created `JSCC` files to the local filesystem. `JSCC` uses the same mechanism of browsers, that allows a `Save as...` in the context menu of a web pages or image. So not uncontrolled write access to your file system is implemented, because users have to select the locations in which the user whats to store the file (e.g. JSON, Javascript or HTML).
* [Font Awesome Icons - 4.7.0](https://fontawesome.com/v4.7.0/icons/) thanks to [fontawesome.com](https://fontawesome.com) for providing the [free 4.7.0 version](https://fontawesome.com/v4.7.0/icons/) for local application for this WebApp. The [fonts in version 4.7.0](https://fontawesome.com/v4.7.0/icons/) are created by ***[Font Awesome](https://fontawesome.com)*** and
licensed under [SIL OFL 1.1](http://scripts.sil.org/OFL). The javascript-code for injecting the icon into the DOM licensed under [MIT License](http://opensource.org/licenses/mit-license.html). The
[Documentation](https://fontawesome.com/v4.7.0/examples/) for [Font Awesome - 4.7.0](https://fontawesome.com/v4.7.0/icons/) licensed under [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/).
* [JointJS](https://github.com/clientIO/joint) JointJS is a JavaScript diagramming library. It can be used to create either static diagrams. JointJS is used in this project to create UML-diagrams, that are interactive diagramming in conjunction and application builder in Javascript.
* [Inheritage for JavaScript with protoypes](http://phrogz.net/js/classes/OOPinJS2.html) by Gavin Kistner
* [3 ways to define a JavaScript class](https://www.phpied.com/3-ways-to-define-a-javascript-class/) by Stoyan Stefanov
* [JQuery](https://jqueryui.com) is used for the theme and standard operations in the Document Object Model (DOM) of HTML-pages. The [JQuery-Themeroller](https://jqueryui.com/themeroller/) was used to create a JQuery theme for JSCC.
* [JSZip](http://stuartk.com/jszip) - LibreOffice files, Geogebra files (Open Source applications) have file extensions that are actually ZIP files. To handle, change and generate those documents in a browser is possible the library JSZIP. Even a small file system for WebApps that can be stored with a folder structure in a ZIP file can be generated in a browser. So [JSZip](http://stuartk.com/jszip) is a multi-functional JavaScript class for generating and reading ZIP files. Thank you for sharing this great library with the Open Source community.
