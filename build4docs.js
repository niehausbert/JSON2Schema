const fs = require("fs");
const pkg = require("./package.json");
const b4c = require("build4code").codegen;

console.log("Start build4docs.js for '" + pkg.name + "' version "+pkg.version);

//-----------------------------------------
b4c.create_header(pkg);
b4c.create_tail(pkg);
//-----------------------------------------
var lib_header = b4c.get_header(pkg);
var outmain = lib_header;

fs.readFile('./docs/js/json2schema.js', 'utf8', function readFileCallback(err, data){
    if (err){
        console.log(err);
    } else {
      var outfile = lib_header;
      outfile += "\n" + data + "\n";
      b4c.save_file('src/libs/json2schema.js', outfile);
      outmain += "\n" + data + "\n";
      fs.readFile('./docs/js/json2template.js', 'utf8', function readFileCallback(err, data){
        if (err){
            console.log(err);
        } else {
          var outtpl = lib_header;
          outtpl += "\n// Library: js/json2template.js\n\n" + data + "\n";
          b4c.save_file('src/libs/json2template.js', outtpl);
          outmain += "\n" + data + "\n";
          fs.readFile('./src/exportvar.js', 'utf8', function readFileCallback(err, data){
            if (err){
                console.log(err);
            } else {
              console.log("exportvar.js:\n"+data);
              outmain += "\n" + data + "\n";
              outmain += b4c.get_tail(pkg);
              b4c.save_file('src/main.js', outmain);
            }
          });
        }
      });

    }
  });
